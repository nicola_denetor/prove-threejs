// creazione scena
var scene = new THREE.Scene();
// creazione camera, con FOV, aspect ratio e clipping plane (vicino e lontano)
var camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.1, 1000 );
// sposto indietro la camera per vedere gli oggetti
camera.position.z = 5;

// creazione renderer
var renderer = new THREE.WebGLRenderer();
// imposto la dimensione del renderer
renderer.setSize( window.innerWidth, window.innerHeight );
// aggiungo il renderer al documento HTML
document.body.appendChild( renderer.domElement );


// creo la forma di un cubo
var geometry = new THREE.BoxGeometry( 1, 1, 1 );
// ci associo un materiale base
var material = new THREE.MeshBasicMaterial( { color: 0x00ff00 } );
// unisco forma e materiale per fare il cubo
var cube = new THREE.Mesh( geometry, material );
// aggiungo il cubo alla scena (in posizione 0,0,0 di default)
scene.add( cube );


// loop di animazione
function animate() {
	requestAnimationFrame( animate );
	// ruoto un po' il cubo (per ogni frame)
	cube.rotation.x += 0.003;
	cube.rotation.y += 0.01;
	renderer.render( scene, camera );
}
animate();


