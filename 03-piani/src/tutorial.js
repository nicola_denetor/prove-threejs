// creazione scena
var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.1, 1000 );
camera.position.set(-30, 60, -60);
camera.lookAt(new THREE.Vector3(0, 0, 0));

// creazione renderer
var renderer = new THREE.WebGLRenderer();
renderer.setSize( window.innerWidth, window.innerHeight );
renderer.shadowMap.enabled = true;
document.body.appendChild( renderer.domElement );


// creazione luce
// var light = new THREE.PointLight( 0xffff00, 1, 500, 0, true, 4*Math.PI );
// light.position.set(10, 50, 10);
// light.castShadow = true;
// //Set up shadow properties for the light
// light.shadow.mapSize.width = 512;  // default
// light.shadow.mapSize.height = 512; // default
// light.shadow.camera.near = 0.5;       // default
// light.shadow.camera.far = 500      // default
// scene.add( light );
// var pointLightHelper = new THREE.PointLightHelper(light, 2);
// scene.add(pointLightHelper);
// Create a helper for the shadow camera (optional)
//scene.add(new THREE.CameraHelper(light.shadow.camera));

var spotLight = new THREE.SpotLight( 0xffffff );
spotLight.position.set(0, 50, 10);
spotLight.castShadow = true;
spotLight.shadow.mapSize.width = 1024;
spotLight.shadow.mapSize.height = 1024;
spotLight.shadow.camera.near = 0.1;
spotLight.shadow.camera.far = 500;
spotLight.shadow.camera.fov = 90;
spotLight.shadow.camera.lookAt(new THREE.Vector3(30,20,30));
spotLight.shadowDarkness = 0.5;
scene.add(spotLight);
scene.add(new THREE.SpotLightHelper(spotLight));


// creazione assi cartesiani
var axesHelper = new THREE.AxesHelper(100);
scene.add(axesHelper);


// creazione caselle schacchiera
var matScuro = new THREE.MeshBasicMaterial( { color: 0x222277, side: THREE.DoubleSide } );
var matChiaro = new THREE.MeshBasicMaterial( { color: 0xccccee, side: THREE.DoubleSide } );
var casella;
for (var x=0; x<8; x++) {
	for (var z=0; z<8; z++) {
		casella = new THREE.Mesh(new THREE.PlaneGeometry(10, 10), ((x+z)%2===0) ? matScuro : matChiaro);
		casella.position.set(x*10, 0, z*10);
		casella.rotation.x = Math.PI / 2;
		casella.castShadow = true;
		casella.receiveShadow = true;
		scene.add(casella);
	}
}


// creazione sfera fluttuante
var sfera = new THREE.Mesh(
	new THREE.SphereGeometry(10, 20, 20),
	new THREE.MeshBasicMaterial( { color: 0x559955 } )
);
sfera.position.set(30, 20, 30);
sfera.castShadow = true;
sfera.receiveShadow = true;
scene.add(sfera);




// loop di animazione
function animate() {
	requestAnimationFrame( animate );
	renderer.render( scene, camera );
}
animate();


