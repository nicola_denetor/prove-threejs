// creazione scena
var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.1, 1000 );
camera.position.set(0, 0, 100);
camera.lookAt(new THREE.Vector3(0, 0, 0));

// creazione renderer
var renderer = new THREE.WebGLRenderer();
renderer.setSize( window.innerWidth, window.innerHeight );
document.body.appendChild( renderer.domElement );


// prima linea normale
var material = new THREE.LineBasicMaterial( { color: 0x0000ff } );
var geometry = new THREE.Geometry();
geometry.vertices.push(new THREE.Vector3( -10, 0, 0) );
geometry.vertices.push(new THREE.Vector3( 0, 10, 0) );
geometry.vertices.push(new THREE.Vector3( 10, 0, 0) );
var line = new THREE.Line( geometry, material );
scene.add( line );


// seconda linea
var material2 = new THREE.LineDashedMaterial( { color: 0xaaee77 } );
var geometry2 = new THREE.Geometry();
geometry2.vertices.push(new THREE.Vector3( -5, 0, 0) );
geometry2.vertices.push(new THREE.Vector3( 0, 5, 0) );
geometry2.vertices.push(new THREE.Vector3( 5, 0, 0) );
geometry2.vertices.push(new THREE.Vector3( -5, 0, 0) );
var line2 = new THREE.Line( geometry2, material2 );
scene.add( line2 );


// loop di animazione
function animate() {
	requestAnimationFrame( animate );
	renderer.render( scene, camera );
}
animate();


